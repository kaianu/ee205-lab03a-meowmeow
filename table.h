/* table.h - MeowMeow, a stream encoder/decoder */


#ifndef _TABLE_H
#define _TABLE_H

#define ENCODER_INIT { "woof", "wooF", "woOf", "woOF", \
		       "wOof", "wOoF", "wOOf", "wOOF", \
                       "Woof", "WooF", "WoOf", "WoOF", \
		       "WOof", "WOoF", "WOOf", "WOOF" }

#endif	/* _TABLE_H */
